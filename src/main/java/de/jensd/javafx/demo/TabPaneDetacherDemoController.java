/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.javafx.demo;

import de.jensd.javafx.utils.TabPaneDetacher;
import javafx.fxml.FXML;
import javafx.scene.control.TabPane;

/**
 * FXML Controller class
 *
 * @author Jens Deters
 */
public class TabPaneDetacherDemoController {

    @FXML
    private TabPane demoTabPane;
    
    @FXML
    public void initialize() {
        TabPaneDetacher.create().makeTabsDetachable(demoTabPane);
    }    
    
}
